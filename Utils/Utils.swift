//
//  Utils.swift
//  C3
//
//  Created by Viko Wijaya on 12/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import AVFoundation
import Speech

class Utils {
    private var appDelegate: AppDelegate!
    private var managedContext: NSManagedObjectContext!
    private var Entity: NSEntityDescription!
    private var Request: NSFetchRequest<NSFetchRequestResult>!
    private var EntityDescription: NSEntityDescription!
    private var ManagedObject: NSManagedObject!
    private var conversationsEntity: NSEntityDescription!
    private var conversationsObject: NSManagedObject!
    private var conversationsRequest: NSFetchRequest<NSFetchRequestResult>!
    private var predicate: NSPredicate!
    
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US")) //1
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private var audioEngine = AVAudioEngine()
    private var audioPlayerSoundEffect: AVAudioPlayer?
    private var botAudioPlayerSoundEffect: AVAudioPlayer?
    
    private var dummyData: DummyData!
    
    private var speechText: String = ""
    private var lang: String = "en-US"
    
    init() {
        
    }
    
    func insertCoreData(entity: String, object: [NSManagedObject]) {
//        print(object)
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        managedContext = appDelegate.persistentContainer.viewContext
        EntityDescription = NSEntityDescription.entity(forEntityName: entity, in: managedContext)!
        
        do {
            ManagedObject = NSManagedObject(entity: EntityDescription, insertInto: managedContext)
            ManagedObject.setValue(object[0].value(forKey: "uid"), forKey: "uid")
            ManagedObject.setValue(object[0].value(forKey: "story_uid"), forKey: "story_uid")
            ManagedObject.setValue(object[0].value(forKey: "speaker"), forKey: "speaker")
            ManagedObject.setValue(object[0].value(forKey: "desc"), forKey: "desc")
            ManagedObject.setValue(object[0].value(forKey: "voice"), forKey: "voice")
            ManagedObject.setValue(object[0].value(forKey: "type"), forKey: "type")
            ManagedObject.setValue(object[0].value(forKey: "next"), forKey: "next")
            ManagedObject.setValue(object[0].value(forKey: "busy"), forKey: "busy")
            ManagedObject.setValue(object[0].value(forKey: "duration"), forKey: "duration")
            
            try managedContext.save()
        } catch let error as NSError {
            print(error)
        }
    }
    
    func migrateConversations() {
        dummyData = DummyData.init()
        
        var arrayConversations = dummyData.conversations()

        appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        managedContext = appDelegate.persistentContainer.viewContext
        conversationsEntity = NSEntityDescription.entity(forEntityName: "Conversations", in: managedContext)!
        
        do {
            for val in 0 ..< arrayConversations.count {
                conversationsObject = NSManagedObject(entity: conversationsEntity, insertInto: managedContext)
                conversationsObject.setValue(arrayConversations[val]["uid"]!, forKey: "uid")
                conversationsObject.setValue(arrayConversations[val]["story_uid"]!, forKey: "story_uid")
                conversationsObject.setValue(arrayConversations[val]["speaker"]!, forKey: "speaker")
                conversationsObject.setValue(arrayConversations[val]["desc"]!, forKey: "desc")
                conversationsObject.setValue(arrayConversations[val]["voice"]!, forKey: "voice")
                conversationsObject.setValue(arrayConversations[val]["type"]!, forKey: "type")
                conversationsObject.setValue(arrayConversations[val]["next"]!, forKey: "next")
                conversationsObject.setValue(arrayConversations[val]["busy"]!, forKey: "busy")
                conversationsObject.setValue(arrayConversations[val]["duration"]!, forKey: "duration")

                try managedContext.save()
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func migrateConversationOptions() {
        dummyData = DummyData.init()
        
        var arrayConversations = dummyData.conversationOptions()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        managedContext = appDelegate.persistentContainer.viewContext
        conversationsEntity = NSEntityDescription.entity(forEntityName: "Options", in: managedContext)!
        
        do {
            for val in 0 ..< arrayConversations.count {
                conversationsObject = NSManagedObject(entity: conversationsEntity, insertInto: managedContext)
                conversationsObject.setValue(arrayConversations[val]["uid"]!, forKey: "uid")
                conversationsObject.setValue(arrayConversations[val]["conv_id"]!, forKey: "conv_id")
                conversationsObject.setValue(arrayConversations[val]["desc"]!, forKey: "desc")
                conversationsObject.setValue(arrayConversations[val]["next"]!, forKey: "next")
                conversationsObject.setValue(arrayConversations[val]["voice"]!, forKey: "voice")
                
                try managedContext.save()
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func fetchCoreData(entityName: String, limit: Int, filter: [String: Int]) -> [NSManagedObject] {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = appDelegate.persistentContainer.viewContext
        
        var object: [NSManagedObject] = []
        
        do {
            Request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            Request.returnsObjectsAsFaults = false
            
            if(filter.count > 0) {
                let filter_key = Array(filter.keys)
                let filter_value = filter[filter_key[0]]!
                let predicate = NSPredicate(format: "\(filter_key[0]) = %@", String(filter_value))

                Request.predicate = predicate
            }
            
            if(limit != 0) {
                Request.fetchLimit = limit
            }
            
            let results = try managedContext.fetch(Request)
            
            object = results as! [NSManagedObject]            
        } catch let error as NSError {
            print(error)
        }
        
        return object
    }
    
    func updateCoreData(entityName: String, limit: Int, filter: [String: Int], dataUpdate: [String: String]) {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            Request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            Request.returnsObjectsAsFaults = false
            
            if(filter.count > 0) {
                let filter_key = Array(filter.keys)
                let filter_value = filter[filter_key[0]]!
                let predicate = NSPredicate(format: "\(filter_key[0]) = %@", String(filter_value))
                
                Request.predicate = predicate
            }
            
            if(limit != 0) {
                Request.fetchLimit = limit
            }
            
            let dataUpdateKey = Array(dataUpdate.keys)
            let dataUpdateValue = dataUpdate[dataUpdateKey[0]]!
            let results = try managedContext.fetch(Request)
            let object = results[0] as! NSManagedObject
            
            object.setValue(dataUpdateValue, forKey: dataUpdateKey[0])
            
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func clearCoreData(entityName: String) {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            Request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            Request.returnsObjectsAsFaults = false
            
            let results = try managedContext.fetch(Request)
            
            for resultsObject in results as! [NSManagedObject] {
                managedContext.delete(resultsObject)
            }
            
            try managedContext.save()
        } catch let error as NSError {
            print(error)
        }
    }
    
    func removeSpecialCharacterForSpeech(text: String) -> String {
        let allowedStr = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=()!_")
        let filteredStr = text.filter {
            allowedStr.contains($0)
        }
        .replacingOccurrences(of: " ", with: "")
        .lowercased()
        
        return changeOKToOkay(text: filteredStr)
    }
    
    func removeSpecialCharacter(text: String) -> String {
        let allowedStr = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=()!_")
        let filteredStr = text.filter {
            allowedStr.contains($0)
            }
            .replacingOccurrences(of: " ", with: "")
            .lowercased()
        
        return filteredStr
    }
    
    func changeOKToOkay(text: String) -> String {
        return text.replacingOccurrences(of: "OK".lowercased(), with: "okay".lowercased())
    }
    
    func playSoundEffect(resource: String) {
        let pathSoundEffect = Bundle.main.path(forResource: resource, ofType: nil)!
        let urlSoundEffect = URL(fileURLWithPath: pathSoundEffect)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.multiRoute, mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            //create your audioPlayer in your parent class as a property
            audioPlayerSoundEffect = try AVAudioPlayer(contentsOf: urlSoundEffect)
            
            guard let player = audioPlayerSoundEffect else { return }
            player.volume = 0.8
            player.play()
        } catch {
            print("couldn't load the file")
        }
    }
    
    func playBotSoundEffect(resource: String) {
        let path = Bundle.main.path(forResource: resource, ofType: nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            //create your audioPlayer in your parent class as a property
            botAudioPlayerSoundEffect = try AVAudioPlayer(contentsOf: url)
            
            guard let player = botAudioPlayerSoundEffect else { return }
            player.volume = 0.8
            player.play()
            //durasi untuk delay perload data
            
            RunLoop.current.run(until: Date()+botAudioPlayerSoundEffect!.duration)
            player.stop()
        } catch {
            print("couldn't load the file")
        }
    }
    
    func startRecording(recordButton: UIButton) {
        //        let audioSession = AVAudioSession.sharedInstance()
        //        do {
        //            try audioSession.setCategory(AVAudioSession.Category.record)
        //            try audioSession.setMode(AVAudioSession.Mode.measurement)
        //            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        //        } catch {
        //            print("audioSession properties weren't set because of an error.")
        //        }
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                self.speechText = result?.bestTranscription.formattedString ?? ""
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                recordButton.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    func stopRecording() -> String {
        //        recognitionTask?.finish()
        //        recognitionTask = nil
        //
        //        recognitionRequest!.endAudio()
        //        audioEngine.stop()
        //        audioEngine.inputNode.removeTap(onBus: 0)
        
        audioEngine.stop()
        recognitionTask?.cancel()
        recognitionRequest?.endAudio()
        
        return speechText
    }
    
    func authorizedSpeech(recordButton: UIButton) {
        speechRecognizer?.delegate = self as? SFSpeechRecognizerDelegate
        speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: lang))
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            default:
                isButtonEnabled = false
            }
            
            OperationQueue.main.addOperation() {
                recordButton.isEnabled = isButtonEnabled
            }
        }
    }
    
    func setGlowingLabel(label: UILabel) {
        label.textColor = #colorLiteral(red: 0.7411764706, green: 0.9529411765, blue: 1, alpha: 1)
        label.layer.shadowColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        label.layer.shadowOffset = .zero
        label.layer.shadowRadius = 2.0
        label.layer.shadowOpacity = 1.0
        label.layer.masksToBounds = false
        label.layer.shouldRasterize = true
    }
}
