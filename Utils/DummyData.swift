//
//  DummyData.swift
//  C3
//
//  Created by Viko Wijaya on 12/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import Foundation

class DummyData {
    
    init() {
        
    }
    
    func conversations() -> [[String: Any ]] {
        // MARK: Conversations "type" digunakan untuk menandakan yang menjawab bot atau user
        let conversations: [[String: Any]]  =
        [
            [
                "uid": "1",
                "story_uid": NSUUID().uuidString,
                "speaker": "Pie Joe",
                "desc": "Hello..? Is anyone there? Hello?",
                "voice": "Uid1.mp3",
                "type": "bot",
                "next": 2,
                "busy": 0,
                "duration" : 0
            ],
            [
                "uid": "2",
                "story_uid": NSUUID().uuidString,
                "speaker": "Pie Joe",
                "desc": "(sigh) I hope this works..",
                "voice": "Uid2.mp3",
                "type": "bot",
                "next": 3,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Yes? who is this? || No, i'm not here
                "uid": "3",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //yes who is this?
                "uid": "4",
                "story_uid": NSUUID().uuidString,
                "speaker": "Pie Joe",
                "desc": "Oh hi.. Just call me PJ-03.",
                "voice": "Uid4.mp3",
                "type": "bot",
                "next": 5,
                "busy": 0,
                "duration" : 0
            ],
            [
                //yes who is this?
                "uid": "5",
                "story_uid": NSUUID().uuidString,
                "speaker": "Pie Joe",
                "desc": "Enough talking. I can only call this number from this weird communication device.",
                "voice": "Uid5.mp3",
                "type": "bot",
                "next": 6,
                "busy": 0,
                "duration" : 0
            ],
            [
                //yes who is this?
                "uid": "6",
                "story_uid": NSUUID().uuidString,
                "speaker": "Pie Joe",
                "desc": "So, you're my only available option for survival. Can you help me?",
                "voice": "Uid6.mp3",
                "type": "bot",
                "next": 48,
                "busy": 0,
                "duration" : 0
            ],
            [
                //No, I'm not here
                "uid": "7",
                "story_uid": NSUUID().uuidString,
                "speaker": "Pie Joe",
                "desc": "Funny. I guess we share the same sense of humor.",
                "voice": "Uid7.mp3",
                "type": "bot",
                "next": 8,
                "busy": 0,
                "duration" : 0
            ],
            [
                //No, I'm not here
                "uid": "8",
                "story_uid": NSUUID().uuidString,
                "speaker": "Pie Joe",
                "desc": "Anyway, I can only call this number from this weird communication device.",
                "voice": "Uid8.mp3",
                "type": "bot",
                "next": 9,
                "busy": 0,
                "duration" : 0
            ],
            [
                //No, I'm not here
                "uid": "9",
                "story_uid": NSUUID().uuidString,
                "speaker": "Pie Joe",
                "desc": "I'm PJ-03 by the way, and you're the only one who can help me right now.",
                "voice": "Uid9.mp3",
                "type": "bot",
                "next": 10,
                "busy": 0,
                "duration" : 0
            ],
            [
                //I dont even know who you are. but okay || Why would i help a stranger?
                "uid": "10",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //i dont even know who you are, but okay
                "uid": "11",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Okay, hear me out. Apparently I got kidnapped and now I'm in a very unfamiliar room",
                "voice": "Uid11.mp3",
                "type": "bot",
                "next": 12,
                "busy": 0,
                "duration" : 0
            ],
            [
                //What can you see? || Do you remember anything at all?
                "uid": "12",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //I don't think so. You're a stranger -- Why would i help a stranger
                "uid": "13",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "I have no time for nonsense.",
                "voice": "Uid13.mp3",
                "type": "bot",
                "next": 14,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Why would i help a stranger
                "uid": "14",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Let's just say.. my life is in your hands. What do you say?",
                "voice": "Uid14.mp3",
                "type": "bot",
                "next": 15,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Fine, what do you want from me? || So i can be a superhero?
                "uid": "15",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //So i can be a superhero?
                "uid": "16",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "If you say so. Oh hail, my superhero.. anyway..",
                "voice": "Uid16.mp3",
                "type": "bot",
                "next": 11,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Do you remember anything at all?
                "uid": "17",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "All I remember is that I was with my wife and son at Limbo-R buying some nutrition pills and S64 drops, and here I am now",
                "voice": "Uid17.mp3",
                "type": "bot",
                "next": 18,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Are you with your wife and son now? || Limbo-R? S64? What are you talking about?
                "uid": "18",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Limbo-R? S64? What are you talking about?
                "uid": "19",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Please get serious. I just need your help to get out of here. Anyway..",
                "voice": "Uid19.mp3",
                "type": "bot",
                "next": 22,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Are you with your wife and son now?
                "uid": "20",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "No, I'm not with them. I hope they're safe, though.",
                "voice": "Uid20.mp3",
                "type": "bot",
                "next": 21,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Are you with your wife and son now?
                "uid": "21",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "All I know is.. I need to get out of here FAST. Anyway..",
                "voice": "Uid21.mp3",
                "type": "bot",
                "next": 22,
                "busy": 0,
                "duration" : 0
            ],
            [
                //What can you see -- Are you with your wife and son now? -- Limbo-R? S64? What are you talking about?
                "uid": "22",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "I'm in a dim room surrounded by empty drawers.",
                "voice": "Uid22.mp3",
                "type": "bot",
                "next": 23,
                "busy": 0,
                "duration" : 0
            ],
            [
                //What can you see -- Are you with your wife and son now? -- Limbo-R? S64? What are you talking about?
                "uid": "23",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Oh wait.. I think I see a door. Is that a door? It looks like one but there's no handle.",
                "voice": "Uid23.mp3",
                "type": "bot",
                "next": 24,
                "busy": 0,
                "duration" : 0
            ],
            [
                //What can you see -- Are you with your wife and son now? -- Limbo-R? S64? What are you talking about?
                "uid": "24",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "There's an EN-817 installed so that must be an access to.. somewhere.",
                "voice": "Uid24.mp3",
                "type": "bot",
                "next": 25,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Go to that door || What is EN-817?
                "uid": "25",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Go to that door
                "uid": "26",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Okay. The EN-817 reads 'THAN BETTER SORRY SAFE'.",
                "voice": "Uid26.mp3",
                "type": "bot",
                "next": 27,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Go to that door
                "uid": "27",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "It must be some type of proverb and we need to unscramble it.",
                "voice": "Uid27.mp3",
                "type": "bot",
                "next": 28,
                "busy": 0,
                "duration" : 0
            ],
            [
                //What do you want me to do with it? || What is EN-817
                "uid": "28",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //What do you want me to do with it?
                "uid": "29",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "I'm still super dizzy right now. Can you help me decipher it?",
                "voice": "Uid29.mp3",
                "type": "bot",
                "next": 49,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Okay, try 1 2 3 4 || What is EN-817?
                "uid": "49",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //What is EN-817? -- Okay, try 1 2 3 4
                "uid": "30",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Seriously, just.. stop joking around.",
                "voice": "Uid30.mp3",
                "type": "bot",
                "next": 31,
                "busy": 0,
                "duration" : 0
            ],
            [
                //What is EN-817? -- Okay, try 1 2 3 4
                "uid": "31",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "If you don't want to help me just say it. I'm really hopeless right now",
                "voice": "Uid31.mp3",
                "type": "bot",
                "next": 32,
                "busy": 0,
                "duration" : 0
            ],
            [
                //IM NOT JOKING AROUND || I'm sorry. I really don't know what it is
                "uid": "32",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //IM NOT JOKING AROUND -- I'm sorry. I really don't know what it is
                "uid": "33",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "You seriously don't know what EN-817 is?",
                "voice": "Uid33.mp3",
                "type": "bot",
                "next": 34,
                "busy": 0,
                "duration" : 0
            ],
            [
                //IM NOT JOKING AROUND -- I'm sorry. I really don't know what it is
                "uid": "34",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "What year are you living in? 2020? Hahaha..",
                "voice": "Uid34.mp3",
                "type": "bot",
                "next": 35,
                "busy": 0,
                "duration" : 0
            ],
            [
                //More like 2019? || Why are you laughing?
                "uid": "35",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //More like 2019? -- Why are you laughing?
                "uid": "36",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "(Hmph) Okay, you know what? I don't want to waste my time talking gibberish with you, so I'll take it.",
                "voice": "Uid36.mp3",
                "type": "bot",
                "next": 37,
                "busy": 0,
                "duration" : 0
            ],
            [
                //More like 2019? -- Why are you laughing?
                "uid": "37",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "But if you haven't noticed, the calendar in this room says it's 2920, for normal people at least",
                "voice": "Uid37.mp3",
                "type": "bot",
                "next": 38,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Are you from a different timeline? || Wow, you're delirious
                "uid": "38",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Are you from different timeline?
                "uid": "39",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Let's just say I am. That will sound cool to you, right?",
                "voice": "Uid39.mp3",
                "type": "bot",
                "next": 40,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Are you from different timeline?
                "uid": "40",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Okay, so just help me with this proverb. Again, it says 'THAN BETTER SORRY SAFE'.",
                "voice": "Uid40.mp3",
                "type": "bot",
                "next": 41,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Are you from different timeline?
                "uid": "41",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Do you have any idea?",
                "voice": "Uid41.mp3",
                "type": "bot",
                "next": 43,
                "busy": 0,
                "duration" : 0
            ],
            [
                //Wow, you're delirious
                "uid": "42",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Okay, whatever you say. Just help me with this proverb. Again, it says 'THAN BETTER SORRY SAFE'.",
                "voice": "Uid42.mp3",
                "type": "bot",
                "next": 41,
                "busy": 0,
                "duration" : 0
            ],
            [
                //BETTER SAFE THAN SORRY || BETTER SORRY THAN SAFE
                "uid": "43",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 1,
                "duration" : 10
            ],
            [
                //BETTER SAFE THAN SORRY
                "uid": "44",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "*EN-817 Unlocks access* PJ-03 moves on to the next room.",
                "voice": "correct.mp3",
                "type": "bot",
                "next": -1,
                "busy": 0,
                "duration" : 0
            ],
            [
                //BETTER SORRY THAN SAFE
                "uid": "45",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "*EN-817 Explodes*",
                "voice": "correct.mp3",
                "type": "bot",
                "next": 46,
                "busy": 0,
                "duration" : 0
            ],
            [
                //BETTER SORRY THAN SAFE
                "uid": "46",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "Btzzz.. tzzz... zzz..",
                "voice": "correct.mp3",
                "type": "bot",
                "next": 47,
                "busy": 0,
                "duration" : 0
            ],
            [
                //BETTER SORRY THAN SAFE
                "uid": "47",
                "story_uid": NSUUID().uuidString,
                "speaker": "bot",
                "desc": "[Connection Lost]",
                "voice": "correct.mp3",
                "type": "bot",
                "next": -1,
                "busy": 0,
                "duration" : 0
            ],
            [
                //I don't even know who you are, but okay || i don't think so. You're a stranger
                "uid": "48",
                "story_uid": NSUUID().uuidString,
                "speaker": "User",
                "desc": "",
                "voice": "",
                "type": "user",
                "next": 0,
                "busy": 0,
                "duration" : 0
            ],
        ]
        
        return conversations
    }
    
    func conversationOptions() -> [[String: Any ]] {
        // MARK: Conversations "type" digunakan untuk menandakan yang menjawab bot atau user
        let convOptions: [[String: Any]] =
        [
            [
                "uid": "1",
                "conv_id": "3",
                "desc": "Yes?  Who is this?",
                "voice": "opUid1.mp3",
                "next": 4
            ],
            [
                "uid": "2",
                "conv_id": "3",
                "desc": "No, I'm not here",
                "voice": "opUid2.mp3",
                "next": 7
            ],
            [
                "uid": "3",
                "conv_id": "48",
                "desc": "I don't even know who you are, but okay.",
                "voice": "opUid3.mp3",
                "next": 11
            ],
            [
                "uid": "4",
                "conv_id": "48",
                "desc": "I don't think so. You're a stranger.",
                "voice": "opUid4.mp3",
                "next": 13
            ],
            [
                "uid": "5",
                "conv_id": "10",
                "desc": "I don't even know who you are, but okay",
                "voice": "opUid5.mp3",
                "next": 11
            ],
            [
                "uid": "6",
                "conv_id": "10",
                "desc": "Why would I help a stranger?",
                "voice": "opUid6.mp3",
                "next": 13
            ],
            [
                "uid": "7",
                "conv_id": "15",
                "desc": "Fine, what do you want from me?",
                "voice": "opUid7.mp3",
                "next": 11
            ],
            [
                "uid": "8",
                "conv_id": "15",
                "desc": "So I can be a superhero?",
                "voice": "opUid8.mp3",
                "next": 16
            ],
            [
                "uid": "9",
                "conv_id": "12",
                "desc": "What can you see?",
                "voice": "opUid9.mp3",
                "next": 22
            ],
            [
                "uid": "10",
                "conv_id": "12",
                "desc": "Do you remember anything at all?",
                "voice": "opUid10.mp3",
                "next": 17
            ],
            [
                "uid": "11",
                "conv_id": "18",
                "desc": "Are you with your wife and son now?",
                "voice": "opUid11.mp3",
                "next": 20
            ],
            [
                "uid": "12",
                "conv_id": "18",
                "desc": "Limbo R? S64? What are you talking about?",
                "voice": "opUid12.mp3",
                "next": 19
            ],
            [
                "uid": "13",
                "conv_id": "25",
                "desc": "Go to that door",
                "voice": "opUid13.mp3",
                "next": 26
            ],
            [
                "uid": "14",
                "conv_id": "25",
                "desc": "What is EN817?",
                "voice": "opUid14.mp3",
                "next": 30
            ],
            [
                "uid": "15",
                "conv_id": "28",
                "desc": "What do you want me to do with it?",
                "voice": "opUid15.mp3",
                "next": 29
            ],
            [
                "uid": "16",
                "conv_id": "28",
                "desc": "What is EN817?",
                "voice": "opUid16.mp3",
                "next": 30
            ],
            [
                "uid": "17",
                "conv_id": "49",
                "desc": "Okay, try 1234",
                "voice": "opUid17.mp3",
                "next": 30
            ],
            [
                "uid": "18",
                "conv_id": "49",
                "desc": "What is EN817?",
                "voice": "opUid18.mp3",
                "next": 30
            ],
            [
                "uid": "19",
                "conv_id": "32",
                "desc": "I am not joking around",
                "voice": "opUid19.mp3",
                "next": 33
            ],
            [
                "uid": "20",
                "conv_id": "32",
                "desc": "I'm sorry. I really don't know what it is",
                "voice": "opUid20.mp3",
                "next": 33
            ],
            [
                "uid": "21",
                "conv_id": "35",
                "desc": "More like 2019?",
                "voice": "opUid21.mp3",
                "next": 36
            ],
            [
                "uid": "22",
                "conv_id": "35",
                "desc": "Why are you laughing?",
                "voice": "opUid22.mp3",
                "next": 36
            ],
            [
                "uid": "23",
                "conv_id": "38",
                "desc": "Are you from a different timeline?",
                "voice": "opUid23.mp3",
                "next": 39
            ],
            [
                "uid": "24",
                "conv_id": "38",
                "desc": "Wow, you're delirious",
                "voice": "opUid24.mp3",
                "next": 42
            ],
            [
                "uid": "25",
                "conv_id": "43",
                "desc": "Better safe than sorry",
                "voice": "opUid25.mp3",
                "next": 44
            ],
            [
                "uid": "26",
                "conv_id": "43",
                "desc": "Better sorry than safe",
                "voice": "opUid26.mp3",
                "next": 45
            ]
        ]
        
        return convOptions
    }
}
