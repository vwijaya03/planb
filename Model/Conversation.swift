//
//  Conversations.swift
//  C3
//
//  Created by Viko Wijaya on 12/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import Foundation

struct Conversation {
    var uid: String
    var story_uid: String
    var speaker: String
    var desc: String
    var voice: String
    var type: String
    var next: Int
    var busy: Int
    var duration: Int
}
