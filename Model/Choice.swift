//
//  Choice.swift
//  C3
//
//  Created by Viko Wijaya on 20/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import Foundation

struct Choice {
    var uid: String
    var conv_id: String
    var desc: String
    var selected: String
    var next: Int
    var voice: String
}
