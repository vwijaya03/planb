//
//  ConversationOptionTableViewCell.swift
//  C3
//
//  Created by Viko Wijaya on 13/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class ConversationOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var option1Button: UIButton!
    @IBOutlet weak var option2Button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
