//
//  CustomUITextView.swift
//  C3
//
//  Created by Viko Wijaya on 10/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class CustomUITextView: UITextView {

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(copy(_:)) ||
//            action == #selector(paste(_:)) ||
//            action == #selector(select(_:)) ||
//            action == #selector(selectAll(_:)) ||
//            action == #selector(cut(_:)) ||
//            action == Selector(("_lookup:")) ||
//            action == Selector(("_share:")) ||
//            action == Selector(("_define:"))
//        {
//            return false
//        }

        if action == #selector(copy(_:)) || action == #selector(selectAll(_:)) || action == #selector(cut(_:)) || action == #selector(paste(_:)) || action == Selector(("_share:")) {
            return false
        }
        
        return super.canPerformAction(action, withSender: sender)
    }

}
