//
//  SelectStoryCollectionViewCell.swift
//  C3
//
//  Created by Sandy Chandra on 19/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class SelectStoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet public weak var storyImageView: UIImageView!
}
