//
//  OptionCell.swift
//  C3
//
//  Created by Viko Wijaya on 21/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class OptionCell: UITableViewCell {

    @IBOutlet weak var leftButton: UILabel!
    @IBOutlet weak var rightButton: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
