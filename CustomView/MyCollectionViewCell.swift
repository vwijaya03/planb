//
//  MyCollectionViewCell.swift
//  C3
//
//  Created by Cosmas Sakristiandio on 24/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var myLabel: UILabel!
    
}
