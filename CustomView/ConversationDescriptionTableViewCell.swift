//
//  ConversationDescriptionTableViewCell.swift
//  C3
//
//  Created by Viko Wijaya on 13/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class ConversationDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var conversationDescriptionTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
