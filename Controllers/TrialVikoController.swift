//
//  TrialVikoController.swift
//  C3
//
//  Created by Viko Wijaya on 12/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit
import Speech

class TrialVikoController: UIViewController {
    
    @IBOutlet weak var cTableView: UITableView!
    @IBOutlet weak var testTextView: CustomUITextView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var convTableView: UITableView!
    
    var idStory = 0
    
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US")) //1
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private var audioEngine = AVAudioEngine()
    private var leftData: Choice = Choice(uid: "", conv_id: "", desc: "", selected: "", next: -1, voice: "")
    private var rightData: Choice = Choice(uid: "", conv_id: "", desc: "", selected: "", next: -1, voice: "")
    private var lang: String = "en-US"
    private var status: String = ""
    private var speechText: String = ""
    private var is_insert: Bool = false
    
    private var utils: Utils!
    private var conversation: [Conversation] = []
    
    private func fetchCOT() {
        let obj = utils.fetchCoreData(entityName: "COT", limit: 0, filter: [:]) as! [COT]
        
        for val in 0 ..< obj.count {
            conversation.append(
                Conversation(
                    uid:        obj[val].uid ?? "",
                    story_uid:  obj[val].story_uid ?? "",
                    speaker:    obj[val].speaker ?? "",
                    desc:       obj[val].desc ?? "",
                    voice:      obj[val].voice ?? "",
                    type:       obj[val].type ?? "",
                    next:       Int(obj[val].next),
                    busy:       Int(obj[val].busy),
                    duration:   Int(obj[val].duration)
                )
            )
        }
        
        if(obj[obj.count-1].type == "user") {
            let convOptObjects = utils.fetchCoreData(entityName: "Options", limit: 0, filter: ["conv_id": Int(obj[obj.count-1].uid!)!])
            
            print(convOptObjects)
        }
        
        fetchConversation(limit: 1, next: Int(obj[obj.count-1].next))
    }

    private func fetchConversation(limit: Int, next: Int) {
        while true {
            RunLoop.current.run(until: Date()+2)
            let obj_cot = utils.fetchCoreData(entityName: "Conversations", limit: limit, filter: ["uid": next])
            let obj = obj_cot as! [Conversations]
//            if(obj_cot.count < 1) {
//                break
//            }
        
            let typeConversation = obj[0].type ?? ""
            
            utils.insertCoreData(entity: "COT", object: obj_cot)
            

            if(typeConversation == "bot") {
                conversation.append(
                    Conversation(
                        uid:        obj[0].uid ?? "",
                        story_uid:  obj[0].story_uid ?? "",
                        speaker:    obj[0].speaker ?? "",
                        desc:       obj[0].desc ?? "",
                        voice:      obj[0].voice ?? "",
                        type:       obj[0].type ?? "",
                        next:       Int(obj[0].next),
                        busy:       Int(obj[0].busy),
                        duration:   Int(obj[0].duration)
                    )
                )
                
//                cTableView.reloadData()
                cTableView.beginUpdates()
                let indexPath = IndexPath(row: conversation.count-1, section: 0)
                cTableView.insertRows(at: [indexPath], with: .top)
                cTableView.endUpdates()
                fetchConversation(limit: 1, next: Int(obj[0].next))
                
                break
            } else {
                conversation.append(
                    Conversation(
                        uid:        obj[0].uid ?? "",
                        story_uid:  obj[0].story_uid ?? "",
                        speaker:    obj[0].speaker ?? "",
                        desc:       obj[0].desc ?? "",
                        voice:      obj[0].voice ?? "",
                        type:       obj[0].type ?? "",
                        next:       Int(obj[0].next),
                        busy:       Int(obj[0].busy),
                        duration:   Int(obj[0].duration)
                    )
                )
                
                let convOptObjects = utils.fetchCoreData(entityName: "Options", limit: 0, filter: ["conv_id": Int(obj[obj.count-1].uid!)!]) as! [Options]
                
                leftData.uid = convOptObjects[0].uid ?? ""
                leftData.conv_id = convOptObjects[0].conv_id ?? ""
                leftData.desc = convOptObjects[0].desc ?? ""
                leftData.selected = convOptObjects[0].selected ?? ""
                leftData.next = Int(convOptObjects[0].next)

                rightData.uid = convOptObjects[1].uid ?? ""
                rightData.conv_id = convOptObjects[1].conv_id ?? ""
                rightData.desc = convOptObjects[1].desc ?? ""
                rightData.selected = convOptObjects[1].selected ?? ""
                rightData.next = Int(convOptObjects[1].next)
                
//                cTableView.reloadData()
                cTableView.beginUpdates()
                let indexPath = IndexPath(row: conversation.count-1, section: 0)
                cTableView.insertRows(at: [indexPath], with: .top)
                cTableView.endUpdates()
                
                break
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        utils = Utils.init()
        utils.clearCoreData(entityName: "COT")
        utils.clearCoreData(entityName: "Conversations")
        utils.clearCoreData(entityName: "Options")
        utils.migrateConversations()
        utils.migrateConversationOptions()
        
        let obj = utils.fetchCoreData(entityName: "COT", limit: 0, filter: [:])

        if(obj.count < 1) {
            fetchConversation(limit: 1, next: 1)
        } else {
            fetchCOT()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cTableView.allowsSelection = false
        
        // Do any additional setup after loading the view.
        recordButton.isEnabled = false
        
        speechRecognizer?.delegate = self as? SFSpeechRecognizerDelegate
        speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: lang))
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            default:
                isButtonEnabled = false
            }
            
            OperationQueue.main.addOperation() {
                self.recordButton.isEnabled = isButtonEnabled
            }
        }
        
        testTextView.text = """
        I want to go to Pakuwon Mall, and i want to eat a pancake and drink a apple juice.
        
        And if possible, i want to watch some movie too.
        """
        testTextView.isEditable = false
        
        let search = UIMenuItem(title: "Jelajah", action: #selector(explore))
        UIMenuController.shared.menuItems = [search]
        UIMenuController.shared.update()
        
        recordButton.addTarget(self, action: #selector(release), for: .touchUpOutside)
        recordButton.addTarget(self, action: #selector(release), for: .touchUpInside)
        recordButton.addTarget(self, action: #selector(hold), for: .touchDown)
    }
    
    @objc func hold(sender: UIButton) {
        startRecording()
        recordButton.setTitle("Stop Recording", for: .normal)
    }
    
    @objc func release(sender: UIButton) {
        audioEngine.stop()
        recognitionRequest!.endAudio()
        recordButton.isEnabled = false
        recordButton.setTitle("Press To Record", for: .normal)
        
        let indexPath = IndexPath(row: conversation.count - 1, section: 0)
        let _ = cTableView.cellForRow(at: indexPath) as! ConversationOptionTableViewCell
        
        print("speech: "+utils.removeSpecialCharacterForSpeech(text: speechText))
        print("left: "+utils.removeSpecialCharacter(text: leftData.desc))
        print("right: "+utils.removeSpecialCharacter(text: rightData.desc))
        
        if(utils.removeSpecialCharacter(text: leftData.desc) == utils.removeSpecialCharacterForSpeech(text: speechText)) {
            print(leftData.next)
            fetchConversation(limit: 1, next: leftData.next)
        } else if(utils.removeSpecialCharacter(text: rightData.desc) == utils.removeSpecialCharacterForSpeech(text: speechText)) {
            print(rightData.next)
            fetchConversation(limit: 1, next: rightData.next)
        }
    }
    
    func startRecording() {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                self.speechText = result?.bestTranscription.formattedString ?? ""
                self.testTextView.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.recordButton.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        testTextView.text = "Say something, I'm listening!"
        
    }
    
    @objc func explore(_ sender: Any) {
        if let textRange = testTextView.selectedTextRange {
            
            let selectedText = testTextView.text(in: textRange)
            
            print(selectedText!)
        }
    }
    
//    func checkAnswer(textSpeech: [Substring]) {
//        let dummySubOption1 = self.dummyOption1.split(separator: " ")
//        let dummySubOption2 = self.dummyOption2.split(separator: " ")
//
//        if(textSpeech.elementsEqual(dummySubOption1)) {
//            //            resultLabel.text = "Pilihan 1"
//        }
//        else if(textSpeech.elementsEqual(dummySubOption2)) {
//            //            resultLabel.text = "Pilihan 2"
//        }
//        else {
//            //            resultLabel.text = "Tidak ada pilihan"
//        }
//    }
}

extension TrialVikoController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let conversationData = conversation[indexPath.row]
        
        if(conversationData.type == "bot") {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationDescriptionTableViewCell", for: indexPath) as! ConversationDescriptionTableViewCell
            cell.conversationDescriptionTextView.text = conversationData.desc
            cell.conversationDescriptionTextView.isEditable = false
            
            return cell
        } else if(conversationData.type == "user") {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationOptionTableViewCell", for: indexPath) as! ConversationOptionTableViewCell
            cell.option1Button.setTitle(leftData.desc, for: .normal)
            cell.option2Button.setTitle(rightData.desc, for: .normal)
            
            return cell
        } else {
            return UITableViewCell()
        }
        
    }
}

