//
//  SelectStoryViewController.swift
//  C3
//
//  Created by Sandy Chandra on 19/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit
import AVFoundation

class SelectStoryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private var storyArray: [[String: Any]] =
        [
            [
                "uid": "1",
                "title": "Time's up",
                "desc": "A former marine sergeant is held hostage in an abandoned utopian old town. His only chance of survival is an old communication device.",
                "img": "Story1_img.png",
                "bgImg": "BGStory1.png",
                "status": true
            ],
            [
                "uid": "2",
                "title": "Coming Soon",
                "desc": "Please wait, we still searching a person that need your help",
                "img": "Story1.png",
                "bgImg": "BackgroundMenu.png",
                "status": false
            ],
            [
                "uid": "3",
                "title": "Coming Soon",
                "desc": "Please wait, we still searching a person that need your help",
                "img": "Story1.png",
                "bgImg": "BackgroundMenu.png",
                "status": false
            ]
        ]
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var selectPageControl: UIPageControl!
    @IBOutlet weak var topTitleLabel: UILabel!
    
    private var imageView : UIImageView!
    
    @IBOutlet public weak var selectCollectionView: UICollectionView!
    
    private var indexOfCellBeforeDragging = 0
    private var selectedStory = 0
    
    @IBOutlet public weak var collectionViewLayout: UICollectionViewFlowLayout!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storyArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectStory", for: indexPath) as! SelectStoryCollectionViewCell
        cell.storyImageView.image = UIImage(named: storyArray[indexPath.row]["img"] as! String)
        
        cell.layer.shadowColor = #colorLiteral(red: 0.1616024971, green: 0.1755575836, blue: 0.1942702532, alpha: 1)
        cell.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shouldRasterize = true
//        cell.storyImageView.layer.cornerRadius = 10
//        cell.storyImageView.layer.masksToBounds = true
        
        return cell
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    func config(){
        assignbackground(name: "BGStory1.png")
        
        titleLabel.text = storyArray[0]["title"] as! String
        descLabel.text = storyArray[0]["desc"] as! String
        selectPageControl.numberOfPages = storyArray.count
        
        setGlowingLabel(label: titleLabel)
        setGlowingLabel(label: descLabel)
        setGlowingLabel(label: topTitleLabel)
    }
    
    func setGlowingLabel(label: UILabel) {
        label.textColor = #colorLiteral(red: 0.7411764706, green: 0.9529411765, blue: 1, alpha: 1)
        label.layer.shadowColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        label.layer.shadowOffset = .zero
        label.layer.shadowRadius = 2.0
        label.layer.shadowOpacity = 1.0
        label.layer.masksToBounds = false
        label.layer.shouldRasterize = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func assignbackground(name: String){
        let background = UIImage(named: name)
        
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    func changeBackground(name: String) {
        imageView.image = UIImage(named: name)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureCollectionViewLayoutItemSize()
    }
    
    @IBAction func startStory(_ sender: UIButton) {
        //pindah halaman ke start gamenya
        performSegue(withIdentifier: "startStory", sender: self)
//        playSaveSound()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is TrialVikoController
        {
            //as? nama next controllernya
            let vc = segue.destination as? TrialVikoController
            vc?.idStory = selectedStory
        }
    }
    
    func calculateSectionInset() -> CGFloat {
        let deviceIsIpad = UIDevice.current.userInterfaceIdiom == .pad
        let deviceOrientationIsLandscape = UIDevice.current.orientation.isLandscape
        let cellBodyViewIsExpended = deviceIsIpad || deviceOrientationIsLandscape
        let cellBodyWidth: CGFloat = 400 + (cellBodyViewIsExpended ? 174 : 0)
        
        let buttonWidth: CGFloat = 50
        
        let inset = (collectionViewLayout.collectionView!.frame.width - cellBodyWidth + buttonWidth) / 4
        return inset
    }
    
    private func configureCollectionViewLayoutItemSize() {
        let inset: CGFloat = calculateSectionInset()
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        
        collectionViewLayout.itemSize = CGSize(width: collectionViewLayout.collectionView!.frame.size.width - inset * 2, height: collectionViewLayout.collectionView!.frame.size.height)
    }
    
    private func indexOfMajorCell() -> Int {
        let itemWidth = collectionViewLayout.itemSize.width
        let proportionalOffset = collectionViewLayout.collectionView!.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        let numberOfItems = selectCollectionView.numberOfItems(inSection: 0)
        let safeIndex = max(0, min(numberOfItems - 1, index))
        return safeIndex
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset
        
        // calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()
        
        let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
            collectionViewLayout.collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        selectedStory = indexPath.row
        titleLabel.text = storyArray[indexPath.row]["title"] as! String
        descLabel.text = storyArray[indexPath.row]["desc"] as! String
        selectPageControl.currentPage = indexPath.row
        
        changeBackground(name: storyArray[indexPath.row]["bgImg"] as! String)
        
        if(storyArray[indexPath.row]["status"] as! Bool) {
            let image = UIImage(named: "Start.png") as UIImage?
            startButton.setBackgroundImage(image, for: [])
            startButton.isEnabled = true
        } else {
            let image = UIImage(named: "Locked.png") as UIImage?
            startButton.setBackgroundImage(image, for: [])
            startButton.isEnabled = false
        }
    }
    
    //play sound untuk conversation + option
    
    var audioPlayer: AVAudioPlayer?
    
    func playSaveSound(){
        let path = Bundle.main.path(forResource: "Uid1.mp3", ofType: nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
            //create your audioPlayer in your parent class as a property
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            
            //durasi untuk delay perload data
            print(audioPlayer?.duration)
            
            audioPlayer!.play()
        } catch {
            print("couldn't load the file")
        }
    }
}
