//
//  StoryViewController.swift
//  C3
//
//  Created by Sandy Chandra on 12/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit
import Speech
import AVKit
import Foundation
import AVFoundation

class StoryViewController: UIViewController, SFSpeechRecognizerDelegate, UITableViewDelegate, UITableViewDataSource {

    
    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //------------------------------------------------------------------------------
    
    @IBOutlet weak var btnStart             : UIButton!
    @IBOutlet weak var lblText              : UILabel!
    
    
    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //------------------------------------------------------------------------------
    
    let speechRecognizer        = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    
    var recognitionRequest      : SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask         : SFSpeechRecognitionTask?
    let audioEngine             = AVAudioEngine()
    
    
    //my variable
    let readText = "Hello hello"
    var speechToText = ""
    var checkResult = false
    var tempArray: [Substring] = [""]
    
    //variable dummy
    var dummyOption1 = "Hello hello hello"
    var dummyOption2 = "Hello hello"
    
    @IBOutlet weak var storyTableView: UITableView!
    
    //    var arrayStory =
    //    [
    //        ["Sir Ernest Shackleton’s Antarctic expedition of 1914 would ultimately fail", "0"],
    //        ["but the hardy crew he mustered would still win honour ", "1"],
    //        ["and recognition for its ability to survive against the odds","2"],
    //        ["Sir Ernest Shackleton’s Antarctic expedition of 1914 would ultimately fail", "0"],
    //        ["but the hardy crew he mustered would still win honour ", "1"],
    //        ["and recognition for its ability to survive against the odds","2"],
    //        ["Sir Ernest Shackleton’s Antarctic expedition of 1914 would ultimately fail", "0"],
    //        ["but the hardy crew he mustered would still win honour ", "1"],
    //        ["and recognition for its ability to survive against the odds","2"],
    //        ["Sir Ernest Shackleton’s Antarctic expedition of 1914 would ultimately fail", "0"],
    //        ["but the hardy crew he mustered would still win honour ", "1"],
    //        ["and recognition for its ability to survive against the odds","2"]
    //    ]
    var arrayStory =
        [
            ["111111", "0"],
            ["2222", "0"],
            ["3333", "1"],
            ["4444", "0"],
            ["but the hardy crew he mustered would still win honour ", "1"],
            ["and recognition for its ability to survive against the odds","0"],
            ["Sir Ernest Shackleton’s Antarctic expedition of 1914 would ultimately fail", "0"],
            ["but the hardy crew he mustered would still win honour ", "1"],
            ["and recognition for its ability to survive against the odds","0"],
            ["Sir Ernest Shackleton’s Antarctic expedition of 1914 would ultimately fail", "0"],
            ["but the hardy crew he mustered would still win honour ", "1"],
            ["and recognition for its ability to survive against the odds","0"]
    ]
    var tempArrayStory: [[String]] = []
   
    
    @IBAction func btnStartSpeechToText(_ sender: UIButton) {

        if audioEngine.isRunning {
            self.audioEngine.stop()
            self.recognitionRequest?.endAudio()
            self.btnStart.isEnabled = false
            self.btnStart.setTitle("Start Recording", for: .normal)
            //            print(self.tempArray)
            checkAnswer(textSpeech: self.tempArray)

        } else {
            self.startRecording()
            self.btnStart.setTitle("Stop Recording", for: .normal)
        }

    }
    
    
    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //------------------------------------------------------------------------------
    
    func setupSpeech() {
        
        self.btnStart.isEnabled = false
        self.speechRecognizer?.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                self.btnStart.isEnabled = isButtonEnabled
            }
        }
        
//        self.btnStart.addTarget(self, action: #selector(release), for: .touchUpOutside)
//        self.btnStart.addTarget(self, action: #selector(release), for: .touchUpInside)
//        self.btnStart.addTarget(self, action: #selector(hold), for: .touchDown)
    }
    
    //------------------------------------------------------------------------------
    
    func startRecording() {
        
        // Clear all previous session data and cancel task
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        // Create instance of audio session to record voice
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: AVAudioSession.Mode.measurement, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        self.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        self.recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                //set hasil speech ke labelx
                self.lblText.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
                
                self.speechToText = self.lblText.text!;
                
                self.checkResult = true
                
                if self.checkResult == true {
                    //mengubah hasil text to speech ke array
                    self.tempArray = self.textToArray(textSpeech: self.speechToText)
                }
            }
            
            //kondisi dimana selesai record
            if error != nil || isFinal {
                
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.btnStart.isEnabled = true
                //                print(self.tempArray)
            }
            
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        self.audioEngine.prepare()
        
        do {
            try self.audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        //set text awal jika tombol record ditekan
        self.lblText.text = "Say something, I'm listening!"
        
        
    }
    
    
    //------------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSpeech()
        storyTableView.separatorStyle = .none
        //        storyTableView.allowsSelection = false;
        
    }
    
    //my function
    func textToArray(textSpeech: String) -> [Substring] {
        let everyWords = textSpeech.split(separator: " ")
        return everyWords
    }
    
    func checkAnswer(textSpeech: [Substring]) {
        let dummySubOption1 = self.dummyOption1.split(separator: " ")
        let dummySubOption2 = self.dummyOption2.split(separator: " ")
        
        if(textSpeech.elementsEqual(dummySubOption1)) {
            //            resultLabel.text = "Pilihan 1"
        }
        else if(textSpeech.elementsEqual(dummySubOption2)) {
            //            resultLabel.text = "Pilihan 2"
        }
        else {
            //            resultLabel.text = "Tidak ada pilihan"
        }
    }
    
    
    //Table View Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tempArrayStory.count
        //        return self.arrayStory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tempArrayStory[indexPath.row][1] == "1") {
            let messageCell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionsTableViewCell
            messageCell.backgroundView = UIImageView(image: UIImage(named: "chatbox.png")!)
            return messageCell
            
        } else if (tempArrayStory[indexPath.row][1] == "0") {
            let messageCell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as! MessageTableViewCell
            messageCell.backgroundView = UIImageView(image: UIImage(named: "chatbox.png")!)
            
            messageCell.MessageLabel.text = self.tempArrayStory[indexPath.row][0]
            return messageCell
            
        } else { return UITableViewCell() }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    @IBAction func plusClicked(_ sender: Any) {
        addMessageToStory(id: tempArrayStory.count)
        playSaveSound()
    }
    
    func addMessageToStory(id: Int){
        
        self.storyTableView.beginUpdates()
        //        print(id)
        
        self.tempArrayStory.insert(self.arrayStory[id], at: id)
        let indexPath = IndexPath(row: id, section: 0)
        self.storyTableView.insertRows(at: [indexPath], with: .top)
        self.storyTableView.endUpdates()
        //        self.storyTableView.reloadData()
    }
    
    var audioPlayer: AVAudioPlayer?
    
    func playSaveSound(){
        let path = Bundle.main.path(forResource: "bell-ringing-01.mp3", ofType: nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
            //create your audioPlayer in your parent class as a property
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer!.play()
        } catch {
            print("couldn't load the file")
        }
    }

}
