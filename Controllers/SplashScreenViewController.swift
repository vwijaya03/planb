//
//  SplashScreenViewController.swift
//  C3
//
//  Created by Viko Wijaya on 10/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

    private var imageView : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground(name: "splash_screen.png")
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            self.performSegue(withIdentifier: "selectStory", sender: nil)
        }
    }
    
    func assignbackground(name: String){
        let background = UIImage(named: name)
        
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
