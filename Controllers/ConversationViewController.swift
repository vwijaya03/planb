//
//  ConversationViewController.swift
//  C3
//
//  Created by Viko Wijaya on 21/06/19.
//  Copyright © 2019 Viko Wijaya. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

class ConversationViewController: UIViewController {

    @IBOutlet var conversationMainView: UIView!
    @IBOutlet weak var conversationTableView: UITableView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US")) //1
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private var audioEngine = AVAudioEngine()
    private var audioPlayer: AVAudioPlayer?
    private var audioPlayerBgSound: AVAudioPlayer?
    private var audioPlayerEffectSound: AVAudioPlayer?
    private var leftData: [Choice] = []
    private var rightData: [Choice] = []
    private var lang: String = "en-US"
    private var speechText: String = ""
    
    private var utils: Utils!
    private var conversation: [Conversation] = []
    
    var effectAudioPlayer: AVPlayer?
    
    override func viewWillAppear(_ animated: Bool) {
        recordButton.isUserInteractionEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //utils.playSoundEffect(resource: "story.mp3")
        utils.clearCoreData(entityName: "COT")
        utils.clearCoreData(entityName: "Conversations")
        utils.clearCoreData(entityName: "Options")
        utils.migrateConversations()
        utils.migrateConversationOptions()

        audioPlayer = AVAudioPlayer()

        let obj = utils.fetchCoreData(entityName: "COT", limit: 0, filter: [:])

        if(obj.count < 1) {
            fetchConversation(limit: 1, next: 1)
        } else {
            fetchCOT()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        utils = Utils.init()
        utils.authorizedSpeech(recordButton: recordButton)
        conversationTableView.allowsSelection = false        
        recordButton.isEnabled = false
        
        let search = UIMenuItem(title: "Jelajah", action: #selector(explore))
        UIMenuController.shared.menuItems = [search]
        UIMenuController.shared.update()
        
        recordButton.addTarget(self, action: #selector(release), for: .touchUpOutside)
        recordButton.addTarget(self, action: #selector(release), for: .touchUpInside)
        recordButton.addTarget(self, action: #selector(hold), for: .touchDown)
    }
    
    @IBAction func backToSelectStory(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "SelectStory", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectStoryViewController") as! SelectStoryViewController
        present(viewController, animated: true, completion: nil)
    }
    
    @objc func hold(sender: UIButton) {
        utils.playSoundEffect(resource: "micPressed.mp3")
        utils.startRecording(recordButton: recordButton)
    }
    
    @objc func release(sender: UIButton) {
        utils.playSoundEffect(resource: "micReleased.mp3")
        RunLoop.current.run(until: Date()+1.5)
        speechText = utils.stopRecording()
        
        let posOption = leftData.count - 1
        let indexPath = IndexPath(row: conversation.count - 1, section: 0)
        let cell = conversationTableView.cellForRow(at: indexPath) as! OptionCell
        
        if(utils.removeSpecialCharacter(text: leftData[posOption].desc) == utils.removeSpecialCharacterForSpeech(text: speechText)) {
            utils.playSoundEffect(resource: "correct.mp3")
            cell.leftButton.backgroundColor = #colorLiteral(red: 0.6823529412, green: 0.9607843137, blue: 1, alpha: 0.35)
            utils.updateCoreData(entityName: "Options", limit: 1, filter: ["uid" : Int(leftData[posOption].uid)!], dataUpdate: ["selected": "yes"])
            fetchConversation(limit: 1, next: leftData[posOption].next)
            
            return
        } else if(utils.removeSpecialCharacter(text: rightData[posOption].desc) == utils.removeSpecialCharacterForSpeech(text: speechText)) {
            utils.playSoundEffect(resource: "correct.mp3")
            cell.rightButton.backgroundColor = #colorLiteral(red: 0.6811234951, green: 0.9605973363, blue: 1, alpha: 0.35)
            utils.updateCoreData(entityName: "Options", limit: 1, filter: ["uid" : Int(rightData[posOption].uid)!], dataUpdate: ["selected": "yes"])
            fetchConversation(limit: 1, next: rightData[posOption].next)
            
            return
        }
        
        recordButton.isUserInteractionEnabled = true
        
        let alert = UIAlertController(title: "What did you say?", message: "", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            alert.dismiss(animated: true, completion: nil)
        }
        
        utils.playSoundEffect(resource: "wrong.mp3")
    }
    
    @objc func explore(_ sender: Any) {
//        if let textRange = testTextView.selectedTextRange {
//            let selectedText = testTextView.text(in: textRange)
//            print(selectedText!)
//        }
    }
    
    private func fetchCOT() {
        let obj = utils.fetchCoreData(entityName: "COT", limit: 0, filter: [:]) as! [COT]
        
        for val in 0 ..< obj.count {
            conversation.append(
                Conversation(
                    uid:        obj[val].uid ?? "",
                    story_uid:  obj[val].story_uid ?? "",
                    speaker:    obj[val].speaker ?? "",
                    desc:       obj[val].desc ?? "",
                    voice:      obj[val].voice ?? "",
                    type:       obj[val].type ?? "",
                    next:       Int(obj[val].next),
                    busy:       Int(obj[val].busy),
                    duration:   Int(obj[val].duration)
                )
            )
        }
        
//        if(obj[obj.count-1].type == "user") {
//            let convOptObjects = utils.fetchCoreData(entityName: "Options", limit: 0, filter: ["conv_id": Int(obj[obj.count-1].uid!)!])
//
//            print(convOptObjects)
//        }
        
        fetchConversation(limit: 1, next: Int(obj[obj.count-1].next))
    }
    
    private func fetchConversation(limit: Int, next: Int) {
        while true {
            let obj_cot = utils.fetchCoreData(entityName: "Conversations", limit: limit, filter: ["uid": next])
            let obj = obj_cot as! [Conversations]
            
            if(obj_cot.count < 1) {
                break
            }
            
            let typeConversation = obj[0].type ?? ""
            
            utils.insertCoreData(entity: "COT", object: obj_cot)
            
            if(typeConversation == "bot") {
                recordButton.isUserInteractionEnabled = false
                
                conversation.append(Conversation(uid: obj[0].uid ?? "", story_uid: obj[0].story_uid ?? "", speaker: obj[0].speaker ?? "", desc: obj[0].desc ?? "", voice: obj[0].voice ?? "", type: obj[0].type ?? "", next: Int(obj[0].next), busy:       Int(obj[0].busy), duration: Int(obj[0].duration)))
                
                let indexPath = IndexPath(row: conversation.count-1, section: 0)
                
                conversationTableView.beginUpdates()
                conversationTableView.insertRows(at: [indexPath], with: .top)
                conversationTableView.endUpdates()
                conversationTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                
                utils.playBotSoundEffect(resource: obj[0].voice!)
                
                fetchConversation(limit: 1, next: Int(obj[0].next))
                
                break
            } else {
                recordButton.isUserInteractionEnabled = true
                
                conversation.append(Conversation(uid: obj[0].uid ?? "", story_uid: obj[0].story_uid ?? "", speaker: obj[0].speaker ?? "", desc: obj[0].desc ?? "", voice: obj[0].voice ?? "", type: obj[0].type ?? "", next: Int(obj[0].next), busy: Int(obj[0].busy), duration: Int(obj[0].duration)))
                
                let convOptObjects = utils.fetchCoreData(entityName: "Options", limit: 0, filter: ["conv_id": Int(obj[obj.count-1].uid!)!]) as! [Options]
                
                leftData.append(Choice(uid: convOptObjects[0].uid ?? "", conv_id: convOptObjects[0].conv_id ?? "", desc: convOptObjects[0].desc ?? "", selected: convOptObjects[0].selected ?? "", next: Int(convOptObjects[0].next), voice: convOptObjects[0].voice ?? ""))
                
                rightData.append(Choice( uid: convOptObjects[1].uid ?? "", conv_id: convOptObjects[1].conv_id ?? "", desc: convOptObjects[1].desc ?? "", selected: convOptObjects[1].selected ?? "", next: Int(convOptObjects[1].next), voice: convOptObjects[1].voice ?? ""))
                
                let indexPath = IndexPath(row: conversation.count-1, section: 0)
                
                conversationTableView.beginUpdates()
                conversationTableView.insertRows(at: [indexPath], with: .top)
                conversationTableView.endUpdates()
                
                conversationTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)

                break
            }
        }
    }
}

extension ConversationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let conversationData = conversation[indexPath.row]
        
        if(conversationData.type == "bot") {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell", for: indexPath) as! ConversationCell
            cell.setTransparent()
            cell.conversationTextView.text = conversationData.desc
            cell.conversationTextView.isEditable = false
            cell.repeatButton.isUserInteractionEnabled = true
            cell.repeatButton.tag = indexPath.row
            cell.repeatButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.repeatSoundBot(_:))))
            
            return cell
        } else if(conversationData.type == "user") {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCell", for: indexPath) as! OptionCell
            cell.setTransparent()
            
            let convOptObjects = utils.fetchCoreData(entityName: "Options", limit: 0, filter: ["conv_id": Int(conversation[indexPath.row].uid)!]) as! [Options]
            
            cell.leftButton.text = convOptObjects[0].desc ?? ""
            cell.leftButton.isUserInteractionEnabled = true
            cell.leftButton.tag = Int(convOptObjects[0].uid!)!
            cell.leftButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.repeatSoundOptionLeft(_:))))
            
            cell.rightButton.text = convOptObjects[1].desc ?? ""
            cell.rightButton.isUserInteractionEnabled = true
            cell.rightButton.tag = Int(convOptObjects[1].uid!)!
            cell.rightButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.repeatSoundOptionRight(_:))))
            
            let leftButtonSelected = convOptObjects[0].selected ?? ""
            let rightButtonSelected = convOptObjects[1].selected ?? ""
            
            if(leftButtonSelected == "yes") {
                cell.leftButton.backgroundColor = #colorLiteral(red: 0.6823529412, green: 0.9607843137, blue: 1, alpha: 0.35)
            } else {
                cell.leftButton.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            }
            
            if(rightButtonSelected == "yes") {
                cell.rightButton.backgroundColor = #colorLiteral(red: 0.6823529412, green: 0.9607843137, blue: 1, alpha: 0.35)
            } else {
                cell.rightButton.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            }
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    @objc func repeatSoundBot(_ sender: UITapGestureRecognizer) {
        let position = sender.view?.tag ?? 0
        
        utils.playSoundEffect(resource: conversation[position].voice)
    }
    
    @objc func repeatSoundOptionLeft(_ sender: UITapGestureRecognizer) {
        let uid_option = sender.view?.tag ?? 0
        let convOptObjects = utils.fetchCoreData(entityName: "Options", limit: 0, filter: ["uid": uid_option]) as! [Options]
        
        utils.playBotSoundEffect(resource: convOptObjects[0].voice!)
    }
    
    @objc func repeatSoundOptionRight(_ sender: UITapGestureRecognizer) {
        let uid_option = sender.view?.tag ?? 0
        let convOptObjects = utils.fetchCoreData(entityName: "Options", limit: 0, filter: ["uid": uid_option]) as! [Options]
        
        utils.playBotSoundEffect(resource: convOptObjects[0].voice!)
    }
}
